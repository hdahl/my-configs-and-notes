#
# ~/.bashrc
#

# File location: ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# My aliases
alias ls='ls -al --color=auto'
alias neofetch='neofetch '

# color variables
GREEN="\[$(tput setaf 012)\]"
RESET="\[$(tput sgr0)\]"
bold=$(tput bold)
normal=$(tput sgr0)

# prompt
PS1="${GREEN}$bold[\u]@\h \W$ ${RESET}"
#PS1='[\u@\h \W]\$ '

# print to terminal
#neofetch
echo -e '



		 _   _ _   /\  _   _ _
		| |_| |  \/  \| |_| | |
		|  _  | | )/\ \  _  | |_
		|_| |_|__//==\ \| |_|___| 
		       /_/    \_\



		      '          

# echo   $(tput bold) $(tput setaf 012)'			********************'
#echo -e '			* Henning er best! *'
#echo '			********************'
#echo 'TERM = '${TERM} #' ' ${PATH}
# echo -e '
#  _    _   ___   _    _   _    _   _   _    _     __
# | |  | | |  _| | \  | | | \  | | | | | \  | |  / __ \
# | |__| | | |_  |  \ | | |  \ | | | | |  \ | | # /  \_\
# |  __  | |  _| | \ \| | | \ \| | | | | \ \| | I | _ _
# | |  | | | |_  | |\ \ | | |\ \ | | | | |\ \ | \ \__| | 
# |_|  |_| |___| |_| \_ | |_| \_ | |_| |_| \_ |  \_ __/ 
# '
 
