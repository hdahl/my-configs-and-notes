#!/bin/bash

# Updates files in project My_configs_and_notes.
echo 'Copy .bash'
cp /home/henning/.bashrc /home/henning/programming/my_configs_and_notes/.bashrc

echo 'copy /home/henning/.config/openbox/* to /home/henning/programming/my_configs_and_notes/'
cp /home/henning/.config/openbox/* /home/henning/programming/my_configs_and_notes/
