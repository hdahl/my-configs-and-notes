# My personal config files

This is a collection of files made while experimenting with Linux, and git/gitlab.com

### Setup	*04082020* : Arch Linux 
			     OpenBox (directly from tty)
			     xfce2-menu

.autoSrcRes	*Sets screen resolution. xrandr--newmode, --addnode and -s.

.wmmenu	*My welcome menu in bash after login.

autostart	*$HOME/.config/openbox/autostart.

config	*i3 config file.

hdautostart.sh	*Same content as .autostart.

menu.xml	*Openbox config.

rc.xml	*Openbox config.
