#!/bin/bash

# ~/.config/i3/hdautostart.sh
echo "Sette xrander fra hdautoscript" >> ~/mylog

# Forsøker med xrandr igjen
xrandr --newmode "1904x1050_60.00"  166.00  1904 2024 2224 2544  1050 1053 1063 1089 -hsync +vsync
xrandr --addmode Virtual1 1904x1050_60.00
xrandr -s 1904x1050_60.00

# echo "ok" >> ~/mylog


